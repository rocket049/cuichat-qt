package main

/*
#cgo CFLAGS: -pipe -O2 -Wall -W -D_REENTRANT -fPIC -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB
#cgo CXXFLAGS: -pipe -O2 -Wall -W -D_REENTRANT -fPIC -DQT_NO_DEBUG -DQT_GUI_LIB -DQT_CORE_LIB
#cgo CXXFLAGS: -I../../cuichat-qt -I. -I../../../go/src/github.com/therecipe/env_linux_amd64_512/5.12.0/gcc_64/include -I../../../go/src/github.com/therecipe/env_linux_amd64_512/5.12.0/gcc_64/include/QtGui -I../../../go/src/github.com/therecipe/env_linux_amd64_512/5.12.0/gcc_64/include/QtCore -I. -isystem /usr/include/libdrm -I../../../go/src/github.com/therecipe/env_linux_amd64_512/5.12.0/gcc_64/mkspecs/linux-g++
#cgo LDFLAGS: -O1 -Wl,-rpath,/home/fuhz/go/src/github.com/therecipe/env_linux_amd64_512/5.12.0/gcc_64/lib
#cgo LDFLAGS:  -L/home/fuhz/go/src/github.com/therecipe/env_linux_amd64_512/5.12.0/gcc_64/lib -lQt5Gui -lQt5Core -lGL -lpthread
#cgo CFLAGS: -Wno-unused-parameter -Wno-unused-variable -Wno-return-type
#cgo CXXFLAGS: -Wno-unused-parameter -Wno-unused-variable -Wno-return-type
*/
import "C"

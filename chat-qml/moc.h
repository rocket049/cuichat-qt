

#pragma once

#ifndef GO_MOC_c97c40_H
#define GO_MOC_c97c40_H

#include <stdint.h>

#ifdef __cplusplus
class QmlBridgec97c40;
void QmlBridgec97c40_QmlBridgec97c40_QRegisterMetaTypes();
extern "C" {
#endif

struct Moc_PackedString { char* data; long long len; };
struct Moc_PackedList { void* data; long long len; };
void QmlBridgec97c40_SendUserMsg(void* ptr, struct Moc_PackedString msg);
void QmlBridgec97c40_Login(void* ptr, struct Moc_PackedString server, struct Moc_PackedString channel, struct Moc_PackedString nick, struct Moc_PackedString pwd);
void QmlBridgec97c40_ConnectAppendUser(void* ptr);
void QmlBridgec97c40_DisconnectAppendUser(void* ptr);
void QmlBridgec97c40_AppendUser(void* ptr, struct Moc_PackedString name);
void QmlBridgec97c40_ConnectRemoveUser(void* ptr);
void QmlBridgec97c40_DisconnectRemoveUser(void* ptr);
void QmlBridgec97c40_RemoveUser(void* ptr, struct Moc_PackedString name);
void QmlBridgec97c40_ConnectAppendMsg(void* ptr);
void QmlBridgec97c40_DisconnectAppendMsg(void* ptr);
void QmlBridgec97c40_AppendMsg(void* ptr, struct Moc_PackedString sender, struct Moc_PackedString msg);
void QmlBridgec97c40_ConnectSetHeader(void* ptr);
void QmlBridgec97c40_DisconnectSetHeader(void* ptr);
void QmlBridgec97c40_SetHeader(void* ptr, struct Moc_PackedString info);
int QmlBridgec97c40_QmlBridgec97c40_QRegisterMetaType();
int QmlBridgec97c40_QmlBridgec97c40_QRegisterMetaType2(char* typeName);
int QmlBridgec97c40_QmlBridgec97c40_QmlRegisterType();
int QmlBridgec97c40_QmlBridgec97c40_QmlRegisterType2(char* uri, int versionMajor, int versionMinor, char* qmlName);
void* QmlBridgec97c40___dynamicPropertyNames_atList(void* ptr, int i);
void QmlBridgec97c40___dynamicPropertyNames_setList(void* ptr, void* i);
void* QmlBridgec97c40___dynamicPropertyNames_newList(void* ptr);
void* QmlBridgec97c40___findChildren_atList2(void* ptr, int i);
void QmlBridgec97c40___findChildren_setList2(void* ptr, void* i);
void* QmlBridgec97c40___findChildren_newList2(void* ptr);
void* QmlBridgec97c40___findChildren_atList3(void* ptr, int i);
void QmlBridgec97c40___findChildren_setList3(void* ptr, void* i);
void* QmlBridgec97c40___findChildren_newList3(void* ptr);
void* QmlBridgec97c40___findChildren_atList(void* ptr, int i);
void QmlBridgec97c40___findChildren_setList(void* ptr, void* i);
void* QmlBridgec97c40___findChildren_newList(void* ptr);
void* QmlBridgec97c40___children_atList(void* ptr, int i);
void QmlBridgec97c40___children_setList(void* ptr, void* i);
void* QmlBridgec97c40___children_newList(void* ptr);
void* QmlBridgec97c40_NewQmlBridge(void* parent);
void QmlBridgec97c40_DestroyQmlBridge(void* ptr);
void QmlBridgec97c40_DestroyQmlBridgeDefault(void* ptr);
char QmlBridgec97c40_EventDefault(void* ptr, void* e);
char QmlBridgec97c40_EventFilterDefault(void* ptr, void* watched, void* event);
void QmlBridgec97c40_ChildEventDefault(void* ptr, void* event);
void QmlBridgec97c40_ConnectNotifyDefault(void* ptr, void* sign);
void QmlBridgec97c40_CustomEventDefault(void* ptr, void* event);
void QmlBridgec97c40_DeleteLaterDefault(void* ptr);
void QmlBridgec97c40_DisconnectNotifyDefault(void* ptr, void* sign);
void QmlBridgec97c40_TimerEventDefault(void* ptr, void* event);
;

#ifdef __cplusplus
}
#endif

#endif
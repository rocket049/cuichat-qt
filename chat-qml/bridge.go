package main

import (
	"github.com/therecipe/qt/core"
)

type QmlBridge struct {
	core.QObject

	_ func(msg string)                        `slot:"sendUserMsg"`
	_ func(server, channel, nick, pwd string) `slot:"login"`

	_ func(name string)        `signal:"appendUser"`
	_ func(name string)        `signal:"removeUser"`
	_ func(sender, msg string) `signal:"appendMsg"`
	_ func(info string)        `signal:"setHeader"`
}

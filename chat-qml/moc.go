package main

//#include <stdint.h>
//#include <stdlib.h>
//#include <string.h>
//#include "moc.h"
import "C"
import (
	"runtime"
	"unsafe"

	"github.com/therecipe/qt"
	std_core "github.com/therecipe/qt/core"
)

func cGoUnpackString(s C.struct_Moc_PackedString) string {
	if int(s.len) == -1 {
		return C.GoString(s.data)
	}
	return C.GoStringN(s.data, C.int(s.len))
}
func cGoUnpackBytes(s C.struct_Moc_PackedString) []byte {
	if int(s.len) == -1 {
		return []byte(C.GoString(s.data))
	}
	return C.GoBytes(unsafe.Pointer(s.data), C.int(s.len))
}

type QmlBridge_ITF interface {
	std_core.QObject_ITF
	QmlBridge_PTR() *QmlBridge
}

func (ptr *QmlBridge) QmlBridge_PTR() *QmlBridge {
	return ptr
}

func (ptr *QmlBridge) Pointer() unsafe.Pointer {
	if ptr != nil {
		return ptr.QObject_PTR().Pointer()
	}
	return nil
}

func (ptr *QmlBridge) SetPointer(p unsafe.Pointer) {
	if ptr != nil {
		ptr.QObject_PTR().SetPointer(p)
	}
}

func PointerFromQmlBridge(ptr QmlBridge_ITF) unsafe.Pointer {
	if ptr != nil {
		return ptr.QmlBridge_PTR().Pointer()
	}
	return nil
}

func NewQmlBridgeFromPointer(ptr unsafe.Pointer) (n *QmlBridge) {
	if gPtr, ok := qt.Receive(ptr); !ok {
		n = new(QmlBridge)
		n.SetPointer(ptr)
	} else {
		switch deduced := gPtr.(type) {
		case *QmlBridge:
			n = deduced

		case *std_core.QObject:
			n = &QmlBridge{QObject: *deduced}

		default:
			n = new(QmlBridge)
			n.SetPointer(ptr)
		}
	}
	return
}

//export callbackQmlBridgec97c40_Constructor
func callbackQmlBridgec97c40_Constructor(ptr unsafe.Pointer) {
	this := NewQmlBridgeFromPointer(ptr)
	qt.Register(ptr, this)
}

//export callbackQmlBridgec97c40_SendUserMsg
func callbackQmlBridgec97c40_SendUserMsg(ptr unsafe.Pointer, msg C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "sendUserMsg"); signal != nil {
		signal.(func(string))(cGoUnpackString(msg))
	}

}

func (ptr *QmlBridge) ConnectSendUserMsg(f func(msg string)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "sendUserMsg"); signal != nil {
			qt.ConnectSignal(ptr.Pointer(), "sendUserMsg", func(msg string) {
				signal.(func(string))(msg)
				f(msg)
			})
		} else {
			qt.ConnectSignal(ptr.Pointer(), "sendUserMsg", f)
		}
	}
}

func (ptr *QmlBridge) DisconnectSendUserMsg() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "sendUserMsg")
	}
}

func (ptr *QmlBridge) SendUserMsg(msg string) {
	if ptr.Pointer() != nil {
		var msgC *C.char
		if msg != "" {
			msgC = C.CString(msg)
			defer C.free(unsafe.Pointer(msgC))
		}
		C.QmlBridgec97c40_SendUserMsg(ptr.Pointer(), C.struct_Moc_PackedString{data: msgC, len: C.longlong(len(msg))})
	}
}

//export callbackQmlBridgec97c40_Login
func callbackQmlBridgec97c40_Login(ptr unsafe.Pointer, server C.struct_Moc_PackedString, channel C.struct_Moc_PackedString, nick C.struct_Moc_PackedString, pwd C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "login"); signal != nil {
		signal.(func(string, string, string, string))(cGoUnpackString(server), cGoUnpackString(channel), cGoUnpackString(nick), cGoUnpackString(pwd))
	}

}

func (ptr *QmlBridge) ConnectLogin(f func(server string, channel string, nick string, pwd string)) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "login"); signal != nil {
			qt.ConnectSignal(ptr.Pointer(), "login", func(server string, channel string, nick string, pwd string) {
				signal.(func(string, string, string, string))(server, channel, nick, pwd)
				f(server, channel, nick, pwd)
			})
		} else {
			qt.ConnectSignal(ptr.Pointer(), "login", f)
		}
	}
}

func (ptr *QmlBridge) DisconnectLogin() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "login")
	}
}

func (ptr *QmlBridge) Login(server string, channel string, nick string, pwd string) {
	if ptr.Pointer() != nil {
		var serverC *C.char
		if server != "" {
			serverC = C.CString(server)
			defer C.free(unsafe.Pointer(serverC))
		}
		var channelC *C.char
		if channel != "" {
			channelC = C.CString(channel)
			defer C.free(unsafe.Pointer(channelC))
		}
		var nickC *C.char
		if nick != "" {
			nickC = C.CString(nick)
			defer C.free(unsafe.Pointer(nickC))
		}
		var pwdC *C.char
		if pwd != "" {
			pwdC = C.CString(pwd)
			defer C.free(unsafe.Pointer(pwdC))
		}
		C.QmlBridgec97c40_Login(ptr.Pointer(), C.struct_Moc_PackedString{data: serverC, len: C.longlong(len(server))}, C.struct_Moc_PackedString{data: channelC, len: C.longlong(len(channel))}, C.struct_Moc_PackedString{data: nickC, len: C.longlong(len(nick))}, C.struct_Moc_PackedString{data: pwdC, len: C.longlong(len(pwd))})
	}
}

//export callbackQmlBridgec97c40_AppendUser
func callbackQmlBridgec97c40_AppendUser(ptr unsafe.Pointer, name C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "appendUser"); signal != nil {
		signal.(func(string))(cGoUnpackString(name))
	}

}

func (ptr *QmlBridge) ConnectAppendUser(f func(name string)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "appendUser") {
			C.QmlBridgec97c40_ConnectAppendUser(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "appendUser"); signal != nil {
			qt.ConnectSignal(ptr.Pointer(), "appendUser", func(name string) {
				signal.(func(string))(name)
				f(name)
			})
		} else {
			qt.ConnectSignal(ptr.Pointer(), "appendUser", f)
		}
	}
}

func (ptr *QmlBridge) DisconnectAppendUser() {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40_DisconnectAppendUser(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "appendUser")
	}
}

func (ptr *QmlBridge) AppendUser(name string) {
	if ptr.Pointer() != nil {
		var nameC *C.char
		if name != "" {
			nameC = C.CString(name)
			defer C.free(unsafe.Pointer(nameC))
		}
		C.QmlBridgec97c40_AppendUser(ptr.Pointer(), C.struct_Moc_PackedString{data: nameC, len: C.longlong(len(name))})
	}
}

//export callbackQmlBridgec97c40_RemoveUser
func callbackQmlBridgec97c40_RemoveUser(ptr unsafe.Pointer, name C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "removeUser"); signal != nil {
		signal.(func(string))(cGoUnpackString(name))
	}

}

func (ptr *QmlBridge) ConnectRemoveUser(f func(name string)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "removeUser") {
			C.QmlBridgec97c40_ConnectRemoveUser(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "removeUser"); signal != nil {
			qt.ConnectSignal(ptr.Pointer(), "removeUser", func(name string) {
				signal.(func(string))(name)
				f(name)
			})
		} else {
			qt.ConnectSignal(ptr.Pointer(), "removeUser", f)
		}
	}
}

func (ptr *QmlBridge) DisconnectRemoveUser() {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40_DisconnectRemoveUser(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "removeUser")
	}
}

func (ptr *QmlBridge) RemoveUser(name string) {
	if ptr.Pointer() != nil {
		var nameC *C.char
		if name != "" {
			nameC = C.CString(name)
			defer C.free(unsafe.Pointer(nameC))
		}
		C.QmlBridgec97c40_RemoveUser(ptr.Pointer(), C.struct_Moc_PackedString{data: nameC, len: C.longlong(len(name))})
	}
}

//export callbackQmlBridgec97c40_AppendMsg
func callbackQmlBridgec97c40_AppendMsg(ptr unsafe.Pointer, sender C.struct_Moc_PackedString, msg C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "appendMsg"); signal != nil {
		signal.(func(string, string))(cGoUnpackString(sender), cGoUnpackString(msg))
	}

}

func (ptr *QmlBridge) ConnectAppendMsg(f func(sender string, msg string)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "appendMsg") {
			C.QmlBridgec97c40_ConnectAppendMsg(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "appendMsg"); signal != nil {
			qt.ConnectSignal(ptr.Pointer(), "appendMsg", func(sender string, msg string) {
				signal.(func(string, string))(sender, msg)
				f(sender, msg)
			})
		} else {
			qt.ConnectSignal(ptr.Pointer(), "appendMsg", f)
		}
	}
}

func (ptr *QmlBridge) DisconnectAppendMsg() {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40_DisconnectAppendMsg(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "appendMsg")
	}
}

func (ptr *QmlBridge) AppendMsg(sender string, msg string) {
	if ptr.Pointer() != nil {
		var senderC *C.char
		if sender != "" {
			senderC = C.CString(sender)
			defer C.free(unsafe.Pointer(senderC))
		}
		var msgC *C.char
		if msg != "" {
			msgC = C.CString(msg)
			defer C.free(unsafe.Pointer(msgC))
		}
		C.QmlBridgec97c40_AppendMsg(ptr.Pointer(), C.struct_Moc_PackedString{data: senderC, len: C.longlong(len(sender))}, C.struct_Moc_PackedString{data: msgC, len: C.longlong(len(msg))})
	}
}

//export callbackQmlBridgec97c40_SetHeader
func callbackQmlBridgec97c40_SetHeader(ptr unsafe.Pointer, info C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "setHeader"); signal != nil {
		signal.(func(string))(cGoUnpackString(info))
	}

}

func (ptr *QmlBridge) ConnectSetHeader(f func(info string)) {
	if ptr.Pointer() != nil {

		if !qt.ExistsSignal(ptr.Pointer(), "setHeader") {
			C.QmlBridgec97c40_ConnectSetHeader(ptr.Pointer())
		}

		if signal := qt.LendSignal(ptr.Pointer(), "setHeader"); signal != nil {
			qt.ConnectSignal(ptr.Pointer(), "setHeader", func(info string) {
				signal.(func(string))(info)
				f(info)
			})
		} else {
			qt.ConnectSignal(ptr.Pointer(), "setHeader", f)
		}
	}
}

func (ptr *QmlBridge) DisconnectSetHeader() {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40_DisconnectSetHeader(ptr.Pointer())
		qt.DisconnectSignal(ptr.Pointer(), "setHeader")
	}
}

func (ptr *QmlBridge) SetHeader(info string) {
	if ptr.Pointer() != nil {
		var infoC *C.char
		if info != "" {
			infoC = C.CString(info)
			defer C.free(unsafe.Pointer(infoC))
		}
		C.QmlBridgec97c40_SetHeader(ptr.Pointer(), C.struct_Moc_PackedString{data: infoC, len: C.longlong(len(info))})
	}
}

func QmlBridge_QRegisterMetaType() int {
	return int(int32(C.QmlBridgec97c40_QmlBridgec97c40_QRegisterMetaType()))
}

func (ptr *QmlBridge) QRegisterMetaType() int {
	return int(int32(C.QmlBridgec97c40_QmlBridgec97c40_QRegisterMetaType()))
}

func QmlBridge_QRegisterMetaType2(typeName string) int {
	var typeNameC *C.char
	if typeName != "" {
		typeNameC = C.CString(typeName)
		defer C.free(unsafe.Pointer(typeNameC))
	}
	return int(int32(C.QmlBridgec97c40_QmlBridgec97c40_QRegisterMetaType2(typeNameC)))
}

func (ptr *QmlBridge) QRegisterMetaType2(typeName string) int {
	var typeNameC *C.char
	if typeName != "" {
		typeNameC = C.CString(typeName)
		defer C.free(unsafe.Pointer(typeNameC))
	}
	return int(int32(C.QmlBridgec97c40_QmlBridgec97c40_QRegisterMetaType2(typeNameC)))
}

func QmlBridge_QmlRegisterType() int {
	return int(int32(C.QmlBridgec97c40_QmlBridgec97c40_QmlRegisterType()))
}

func (ptr *QmlBridge) QmlRegisterType() int {
	return int(int32(C.QmlBridgec97c40_QmlBridgec97c40_QmlRegisterType()))
}

func QmlBridge_QmlRegisterType2(uri string, versionMajor int, versionMinor int, qmlName string) int {
	var uriC *C.char
	if uri != "" {
		uriC = C.CString(uri)
		defer C.free(unsafe.Pointer(uriC))
	}
	var qmlNameC *C.char
	if qmlName != "" {
		qmlNameC = C.CString(qmlName)
		defer C.free(unsafe.Pointer(qmlNameC))
	}
	return int(int32(C.QmlBridgec97c40_QmlBridgec97c40_QmlRegisterType2(uriC, C.int(int32(versionMajor)), C.int(int32(versionMinor)), qmlNameC)))
}

func (ptr *QmlBridge) QmlRegisterType2(uri string, versionMajor int, versionMinor int, qmlName string) int {
	var uriC *C.char
	if uri != "" {
		uriC = C.CString(uri)
		defer C.free(unsafe.Pointer(uriC))
	}
	var qmlNameC *C.char
	if qmlName != "" {
		qmlNameC = C.CString(qmlName)
		defer C.free(unsafe.Pointer(qmlNameC))
	}
	return int(int32(C.QmlBridgec97c40_QmlBridgec97c40_QmlRegisterType2(uriC, C.int(int32(versionMajor)), C.int(int32(versionMinor)), qmlNameC)))
}

func (ptr *QmlBridge) __dynamicPropertyNames_atList(i int) *std_core.QByteArray {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQByteArrayFromPointer(C.QmlBridgec97c40___dynamicPropertyNames_atList(ptr.Pointer(), C.int(int32(i))))
		runtime.SetFinalizer(tmpValue, (*std_core.QByteArray).DestroyQByteArray)
		return tmpValue
	}
	return nil
}

func (ptr *QmlBridge) __dynamicPropertyNames_setList(i std_core.QByteArray_ITF) {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40___dynamicPropertyNames_setList(ptr.Pointer(), std_core.PointerFromQByteArray(i))
	}
}

func (ptr *QmlBridge) __dynamicPropertyNames_newList() unsafe.Pointer {
	return C.QmlBridgec97c40___dynamicPropertyNames_newList(ptr.Pointer())
}

func (ptr *QmlBridge) __findChildren_atList2(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.QmlBridgec97c40___findChildren_atList2(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *QmlBridge) __findChildren_setList2(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40___findChildren_setList2(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *QmlBridge) __findChildren_newList2() unsafe.Pointer {
	return C.QmlBridgec97c40___findChildren_newList2(ptr.Pointer())
}

func (ptr *QmlBridge) __findChildren_atList3(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.QmlBridgec97c40___findChildren_atList3(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *QmlBridge) __findChildren_setList3(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40___findChildren_setList3(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *QmlBridge) __findChildren_newList3() unsafe.Pointer {
	return C.QmlBridgec97c40___findChildren_newList3(ptr.Pointer())
}

func (ptr *QmlBridge) __findChildren_atList(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.QmlBridgec97c40___findChildren_atList(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *QmlBridge) __findChildren_setList(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40___findChildren_setList(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *QmlBridge) __findChildren_newList() unsafe.Pointer {
	return C.QmlBridgec97c40___findChildren_newList(ptr.Pointer())
}

func (ptr *QmlBridge) __children_atList(i int) *std_core.QObject {
	if ptr.Pointer() != nil {
		tmpValue := std_core.NewQObjectFromPointer(C.QmlBridgec97c40___children_atList(ptr.Pointer(), C.int(int32(i))))
		if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
			tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
		}
		return tmpValue
	}
	return nil
}

func (ptr *QmlBridge) __children_setList(i std_core.QObject_ITF) {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40___children_setList(ptr.Pointer(), std_core.PointerFromQObject(i))
	}
}

func (ptr *QmlBridge) __children_newList() unsafe.Pointer {
	return C.QmlBridgec97c40___children_newList(ptr.Pointer())
}

func NewQmlBridge(parent std_core.QObject_ITF) *QmlBridge {
	tmpValue := NewQmlBridgeFromPointer(C.QmlBridgec97c40_NewQmlBridge(std_core.PointerFromQObject(parent)))
	if !qt.ExistsSignal(tmpValue.Pointer(), "destroyed") {
		tmpValue.ConnectDestroyed(func(*std_core.QObject) { tmpValue.SetPointer(nil) })
	}
	return tmpValue
}

//export callbackQmlBridgec97c40_DestroyQmlBridge
func callbackQmlBridgec97c40_DestroyQmlBridge(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "~QmlBridge"); signal != nil {
		signal.(func())()
	} else {
		NewQmlBridgeFromPointer(ptr).DestroyQmlBridgeDefault()
	}
}

func (ptr *QmlBridge) ConnectDestroyQmlBridge(f func()) {
	if ptr.Pointer() != nil {

		if signal := qt.LendSignal(ptr.Pointer(), "~QmlBridge"); signal != nil {
			qt.ConnectSignal(ptr.Pointer(), "~QmlBridge", func() {
				signal.(func())()
				f()
			})
		} else {
			qt.ConnectSignal(ptr.Pointer(), "~QmlBridge", f)
		}
	}
}

func (ptr *QmlBridge) DisconnectDestroyQmlBridge() {
	if ptr.Pointer() != nil {

		qt.DisconnectSignal(ptr.Pointer(), "~QmlBridge")
	}
}

func (ptr *QmlBridge) DestroyQmlBridge() {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40_DestroyQmlBridge(ptr.Pointer())
		ptr.SetPointer(nil)
		runtime.SetFinalizer(ptr, nil)
	}
}

func (ptr *QmlBridge) DestroyQmlBridgeDefault() {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40_DestroyQmlBridgeDefault(ptr.Pointer())
		ptr.SetPointer(nil)
		runtime.SetFinalizer(ptr, nil)
	}
}

//export callbackQmlBridgec97c40_Event
func callbackQmlBridgec97c40_Event(ptr unsafe.Pointer, e unsafe.Pointer) C.char {
	if signal := qt.GetSignal(ptr, "event"); signal != nil {
		return C.char(int8(qt.GoBoolToInt(signal.(func(*std_core.QEvent) bool)(std_core.NewQEventFromPointer(e)))))
	}

	return C.char(int8(qt.GoBoolToInt(NewQmlBridgeFromPointer(ptr).EventDefault(std_core.NewQEventFromPointer(e)))))
}

func (ptr *QmlBridge) EventDefault(e std_core.QEvent_ITF) bool {
	if ptr.Pointer() != nil {
		return int8(C.QmlBridgec97c40_EventDefault(ptr.Pointer(), std_core.PointerFromQEvent(e))) != 0
	}
	return false
}

//export callbackQmlBridgec97c40_EventFilter
func callbackQmlBridgec97c40_EventFilter(ptr unsafe.Pointer, watched unsafe.Pointer, event unsafe.Pointer) C.char {
	if signal := qt.GetSignal(ptr, "eventFilter"); signal != nil {
		return C.char(int8(qt.GoBoolToInt(signal.(func(*std_core.QObject, *std_core.QEvent) bool)(std_core.NewQObjectFromPointer(watched), std_core.NewQEventFromPointer(event)))))
	}

	return C.char(int8(qt.GoBoolToInt(NewQmlBridgeFromPointer(ptr).EventFilterDefault(std_core.NewQObjectFromPointer(watched), std_core.NewQEventFromPointer(event)))))
}

func (ptr *QmlBridge) EventFilterDefault(watched std_core.QObject_ITF, event std_core.QEvent_ITF) bool {
	if ptr.Pointer() != nil {
		return int8(C.QmlBridgec97c40_EventFilterDefault(ptr.Pointer(), std_core.PointerFromQObject(watched), std_core.PointerFromQEvent(event))) != 0
	}
	return false
}

//export callbackQmlBridgec97c40_ChildEvent
func callbackQmlBridgec97c40_ChildEvent(ptr unsafe.Pointer, event unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "childEvent"); signal != nil {
		signal.(func(*std_core.QChildEvent))(std_core.NewQChildEventFromPointer(event))
	} else {
		NewQmlBridgeFromPointer(ptr).ChildEventDefault(std_core.NewQChildEventFromPointer(event))
	}
}

func (ptr *QmlBridge) ChildEventDefault(event std_core.QChildEvent_ITF) {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40_ChildEventDefault(ptr.Pointer(), std_core.PointerFromQChildEvent(event))
	}
}

//export callbackQmlBridgec97c40_ConnectNotify
func callbackQmlBridgec97c40_ConnectNotify(ptr unsafe.Pointer, sign unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "connectNotify"); signal != nil {
		signal.(func(*std_core.QMetaMethod))(std_core.NewQMetaMethodFromPointer(sign))
	} else {
		NewQmlBridgeFromPointer(ptr).ConnectNotifyDefault(std_core.NewQMetaMethodFromPointer(sign))
	}
}

func (ptr *QmlBridge) ConnectNotifyDefault(sign std_core.QMetaMethod_ITF) {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40_ConnectNotifyDefault(ptr.Pointer(), std_core.PointerFromQMetaMethod(sign))
	}
}

//export callbackQmlBridgec97c40_CustomEvent
func callbackQmlBridgec97c40_CustomEvent(ptr unsafe.Pointer, event unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "customEvent"); signal != nil {
		signal.(func(*std_core.QEvent))(std_core.NewQEventFromPointer(event))
	} else {
		NewQmlBridgeFromPointer(ptr).CustomEventDefault(std_core.NewQEventFromPointer(event))
	}
}

func (ptr *QmlBridge) CustomEventDefault(event std_core.QEvent_ITF) {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40_CustomEventDefault(ptr.Pointer(), std_core.PointerFromQEvent(event))
	}
}

//export callbackQmlBridgec97c40_DeleteLater
func callbackQmlBridgec97c40_DeleteLater(ptr unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "deleteLater"); signal != nil {
		signal.(func())()
	} else {
		NewQmlBridgeFromPointer(ptr).DeleteLaterDefault()
	}
}

func (ptr *QmlBridge) DeleteLaterDefault() {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40_DeleteLaterDefault(ptr.Pointer())
		ptr.SetPointer(nil)
		runtime.SetFinalizer(ptr, nil)
	}
}

//export callbackQmlBridgec97c40_Destroyed
func callbackQmlBridgec97c40_Destroyed(ptr unsafe.Pointer, obj unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "destroyed"); signal != nil {
		signal.(func(*std_core.QObject))(std_core.NewQObjectFromPointer(obj))
	}

}

//export callbackQmlBridgec97c40_DisconnectNotify
func callbackQmlBridgec97c40_DisconnectNotify(ptr unsafe.Pointer, sign unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "disconnectNotify"); signal != nil {
		signal.(func(*std_core.QMetaMethod))(std_core.NewQMetaMethodFromPointer(sign))
	} else {
		NewQmlBridgeFromPointer(ptr).DisconnectNotifyDefault(std_core.NewQMetaMethodFromPointer(sign))
	}
}

func (ptr *QmlBridge) DisconnectNotifyDefault(sign std_core.QMetaMethod_ITF) {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40_DisconnectNotifyDefault(ptr.Pointer(), std_core.PointerFromQMetaMethod(sign))
	}
}

//export callbackQmlBridgec97c40_ObjectNameChanged
func callbackQmlBridgec97c40_ObjectNameChanged(ptr unsafe.Pointer, objectName C.struct_Moc_PackedString) {
	if signal := qt.GetSignal(ptr, "objectNameChanged"); signal != nil {
		signal.(func(string))(cGoUnpackString(objectName))
	}

}

//export callbackQmlBridgec97c40_TimerEvent
func callbackQmlBridgec97c40_TimerEvent(ptr unsafe.Pointer, event unsafe.Pointer) {
	if signal := qt.GetSignal(ptr, "timerEvent"); signal != nil {
		signal.(func(*std_core.QTimerEvent))(std_core.NewQTimerEventFromPointer(event))
	} else {
		NewQmlBridgeFromPointer(ptr).TimerEventDefault(std_core.NewQTimerEventFromPointer(event))
	}
}

func (ptr *QmlBridge) TimerEventDefault(event std_core.QTimerEvent_ITF) {
	if ptr.Pointer() != nil {
		C.QmlBridgec97c40_TimerEventDefault(ptr.Pointer(), std_core.PointerFromQTimerEvent(event))
	}
}

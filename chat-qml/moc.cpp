

#define protected public
#define private public

#include "moc.h"
#include "_cgo_export.h"

#include <QByteArray>
#include <QCameraImageCapture>
#include <QChildEvent>
#include <QDBusPendingCallWatcher>
#include <QEvent>
#include <QExtensionFactory>
#include <QExtensionManager>
#include <QGraphicsObject>
#include <QGraphicsWidget>
#include <QLayout>
#include <QMediaPlaylist>
#include <QMediaRecorder>
#include <QMetaMethod>
#include <QMetaObject>
#include <QObject>
#include <QOffscreenSurface>
#include <QPaintDeviceWindow>
#include <QPdfWriter>
#include <QQuickItem>
#include <QRadioData>
#include <QString>
#include <QTimerEvent>
#include <QWidget>
#include <QWindow>


class QmlBridgec97c40: public QObject
{
Q_OBJECT
public:
	QmlBridgec97c40(QObject *parent = Q_NULLPTR) : QObject(parent) {qRegisterMetaType<quintptr>("quintptr");QmlBridgec97c40_QmlBridgec97c40_QRegisterMetaType();QmlBridgec97c40_QmlBridgec97c40_QRegisterMetaTypes();callbackQmlBridgec97c40_Constructor(this);};
	void Signal_AppendUser(QString name) { QByteArray t6ae999 = name.toUtf8(); Moc_PackedString namePacked = { const_cast<char*>(t6ae999.prepend("WHITESPACE").constData()+10), t6ae999.size()-10 };callbackQmlBridgec97c40_AppendUser(this, namePacked); };
	void Signal_RemoveUser(QString name) { QByteArray t6ae999 = name.toUtf8(); Moc_PackedString namePacked = { const_cast<char*>(t6ae999.prepend("WHITESPACE").constData()+10), t6ae999.size()-10 };callbackQmlBridgec97c40_RemoveUser(this, namePacked); };
	void Signal_AppendMsg(QString sender, QString msg) { QByteArray tacc6a3 = sender.toUtf8(); Moc_PackedString senderPacked = { const_cast<char*>(tacc6a3.prepend("WHITESPACE").constData()+10), tacc6a3.size()-10 };QByteArray t19f34e = msg.toUtf8(); Moc_PackedString msgPacked = { const_cast<char*>(t19f34e.prepend("WHITESPACE").constData()+10), t19f34e.size()-10 };callbackQmlBridgec97c40_AppendMsg(this, senderPacked, msgPacked); };
	void Signal_SetHeader(QString info) { QByteArray t59bd0a = info.toUtf8(); Moc_PackedString infoPacked = { const_cast<char*>(t59bd0a.prepend("WHITESPACE").constData()+10), t59bd0a.size()-10 };callbackQmlBridgec97c40_SetHeader(this, infoPacked); };
	 ~QmlBridgec97c40() { callbackQmlBridgec97c40_DestroyQmlBridge(this); };
	bool event(QEvent * e) { return callbackQmlBridgec97c40_Event(this, e) != 0; };
	bool eventFilter(QObject * watched, QEvent * event) { return callbackQmlBridgec97c40_EventFilter(this, watched, event) != 0; };
	void childEvent(QChildEvent * event) { callbackQmlBridgec97c40_ChildEvent(this, event); };
	void connectNotify(const QMetaMethod & sign) { callbackQmlBridgec97c40_ConnectNotify(this, const_cast<QMetaMethod*>(&sign)); };
	void customEvent(QEvent * event) { callbackQmlBridgec97c40_CustomEvent(this, event); };
	void deleteLater() { callbackQmlBridgec97c40_DeleteLater(this); };
	void Signal_Destroyed(QObject * obj) { callbackQmlBridgec97c40_Destroyed(this, obj); };
	void disconnectNotify(const QMetaMethod & sign) { callbackQmlBridgec97c40_DisconnectNotify(this, const_cast<QMetaMethod*>(&sign)); };
	void Signal_ObjectNameChanged(const QString & objectName) { QByteArray taa2c4f = objectName.toUtf8(); Moc_PackedString objectNamePacked = { const_cast<char*>(taa2c4f.prepend("WHITESPACE").constData()+10), taa2c4f.size()-10 };callbackQmlBridgec97c40_ObjectNameChanged(this, objectNamePacked); };
	void timerEvent(QTimerEvent * event) { callbackQmlBridgec97c40_TimerEvent(this, event); };
signals:
	void appendUser(QString name);
	void removeUser(QString name);
	void appendMsg(QString sender, QString msg);
	void setHeader(QString info);
public slots:
	void sendUserMsg(QString msg) { QByteArray t19f34e = msg.toUtf8(); Moc_PackedString msgPacked = { const_cast<char*>(t19f34e.prepend("WHITESPACE").constData()+10), t19f34e.size()-10 };callbackQmlBridgec97c40_SendUserMsg(this, msgPacked); };
	void login(QString server, QString channel, QString nick, QString pwd) { QByteArray t3de4f9 = server.toUtf8(); Moc_PackedString serverPacked = { const_cast<char*>(t3de4f9.prepend("WHITESPACE").constData()+10), t3de4f9.size()-10 };QByteArray tfbe7d7 = channel.toUtf8(); Moc_PackedString channelPacked = { const_cast<char*>(tfbe7d7.prepend("WHITESPACE").constData()+10), tfbe7d7.size()-10 };QByteArray t75ef9f = nick.toUtf8(); Moc_PackedString nickPacked = { const_cast<char*>(t75ef9f.prepend("WHITESPACE").constData()+10), t75ef9f.size()-10 };QByteArray t37fa26 = pwd.toUtf8(); Moc_PackedString pwdPacked = { const_cast<char*>(t37fa26.prepend("WHITESPACE").constData()+10), t37fa26.size()-10 };callbackQmlBridgec97c40_Login(this, serverPacked, channelPacked, nickPacked, pwdPacked); };
private:
};

Q_DECLARE_METATYPE(QmlBridgec97c40*)


void QmlBridgec97c40_QmlBridgec97c40_QRegisterMetaTypes() {
}

void QmlBridgec97c40_SendUserMsg(void* ptr, struct Moc_PackedString msg)
{
	QMetaObject::invokeMethod(static_cast<QmlBridgec97c40*>(ptr), "sendUserMsg", Q_ARG(QString, QString::fromUtf8(msg.data, msg.len)));
}

void QmlBridgec97c40_Login(void* ptr, struct Moc_PackedString server, struct Moc_PackedString channel, struct Moc_PackedString nick, struct Moc_PackedString pwd)
{
	QMetaObject::invokeMethod(static_cast<QmlBridgec97c40*>(ptr), "login", Q_ARG(QString, QString::fromUtf8(server.data, server.len)), Q_ARG(QString, QString::fromUtf8(channel.data, channel.len)), Q_ARG(QString, QString::fromUtf8(nick.data, nick.len)), Q_ARG(QString, QString::fromUtf8(pwd.data, pwd.len)));
}

void QmlBridgec97c40_ConnectAppendUser(void* ptr)
{
	QObject::connect(static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString)>(&QmlBridgec97c40::appendUser), static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString)>(&QmlBridgec97c40::Signal_AppendUser));
}

void QmlBridgec97c40_DisconnectAppendUser(void* ptr)
{
	QObject::disconnect(static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString)>(&QmlBridgec97c40::appendUser), static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString)>(&QmlBridgec97c40::Signal_AppendUser));
}

void QmlBridgec97c40_AppendUser(void* ptr, struct Moc_PackedString name)
{
	static_cast<QmlBridgec97c40*>(ptr)->appendUser(QString::fromUtf8(name.data, name.len));
}

void QmlBridgec97c40_ConnectRemoveUser(void* ptr)
{
	QObject::connect(static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString)>(&QmlBridgec97c40::removeUser), static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString)>(&QmlBridgec97c40::Signal_RemoveUser));
}

void QmlBridgec97c40_DisconnectRemoveUser(void* ptr)
{
	QObject::disconnect(static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString)>(&QmlBridgec97c40::removeUser), static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString)>(&QmlBridgec97c40::Signal_RemoveUser));
}

void QmlBridgec97c40_RemoveUser(void* ptr, struct Moc_PackedString name)
{
	static_cast<QmlBridgec97c40*>(ptr)->removeUser(QString::fromUtf8(name.data, name.len));
}

void QmlBridgec97c40_ConnectAppendMsg(void* ptr)
{
	QObject::connect(static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString, QString)>(&QmlBridgec97c40::appendMsg), static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString, QString)>(&QmlBridgec97c40::Signal_AppendMsg));
}

void QmlBridgec97c40_DisconnectAppendMsg(void* ptr)
{
	QObject::disconnect(static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString, QString)>(&QmlBridgec97c40::appendMsg), static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString, QString)>(&QmlBridgec97c40::Signal_AppendMsg));
}

void QmlBridgec97c40_AppendMsg(void* ptr, struct Moc_PackedString sender, struct Moc_PackedString msg)
{
	static_cast<QmlBridgec97c40*>(ptr)->appendMsg(QString::fromUtf8(sender.data, sender.len), QString::fromUtf8(msg.data, msg.len));
}

void QmlBridgec97c40_ConnectSetHeader(void* ptr)
{
	QObject::connect(static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString)>(&QmlBridgec97c40::setHeader), static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString)>(&QmlBridgec97c40::Signal_SetHeader));
}

void QmlBridgec97c40_DisconnectSetHeader(void* ptr)
{
	QObject::disconnect(static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString)>(&QmlBridgec97c40::setHeader), static_cast<QmlBridgec97c40*>(ptr), static_cast<void (QmlBridgec97c40::*)(QString)>(&QmlBridgec97c40::Signal_SetHeader));
}

void QmlBridgec97c40_SetHeader(void* ptr, struct Moc_PackedString info)
{
	static_cast<QmlBridgec97c40*>(ptr)->setHeader(QString::fromUtf8(info.data, info.len));
}

int QmlBridgec97c40_QmlBridgec97c40_QRegisterMetaType()
{
	return qRegisterMetaType<QmlBridgec97c40*>();
}

int QmlBridgec97c40_QmlBridgec97c40_QRegisterMetaType2(char* typeName)
{
	return qRegisterMetaType<QmlBridgec97c40*>(const_cast<const char*>(typeName));
}

int QmlBridgec97c40_QmlBridgec97c40_QmlRegisterType()
{
#ifdef QT_QML_LIB
	return qmlRegisterType<QmlBridgec97c40>();
#else
	return 0;
#endif
}

int QmlBridgec97c40_QmlBridgec97c40_QmlRegisterType2(char* uri, int versionMajor, int versionMinor, char* qmlName)
{
#ifdef QT_QML_LIB
	return qmlRegisterType<QmlBridgec97c40>(const_cast<const char*>(uri), versionMajor, versionMinor, const_cast<const char*>(qmlName));
#else
	return 0;
#endif
}

void* QmlBridgec97c40___dynamicPropertyNames_atList(void* ptr, int i)
{
	return new QByteArray(({QByteArray tmp = static_cast<QList<QByteArray>*>(ptr)->at(i); if (i == static_cast<QList<QByteArray>*>(ptr)->size()-1) { static_cast<QList<QByteArray>*>(ptr)->~QList(); free(ptr); }; tmp; }));
}

void QmlBridgec97c40___dynamicPropertyNames_setList(void* ptr, void* i)
{
	static_cast<QList<QByteArray>*>(ptr)->append(*static_cast<QByteArray*>(i));
}

void* QmlBridgec97c40___dynamicPropertyNames_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QByteArray>();
}

void* QmlBridgec97c40___findChildren_atList2(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void QmlBridgec97c40___findChildren_setList2(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* QmlBridgec97c40___findChildren_newList2(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* QmlBridgec97c40___findChildren_atList3(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void QmlBridgec97c40___findChildren_setList3(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* QmlBridgec97c40___findChildren_newList3(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* QmlBridgec97c40___findChildren_atList(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void QmlBridgec97c40___findChildren_setList(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* QmlBridgec97c40___findChildren_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* QmlBridgec97c40___children_atList(void* ptr, int i)
{
	return ({QObject * tmp = static_cast<QList<QObject *>*>(ptr)->at(i); if (i == static_cast<QList<QObject *>*>(ptr)->size()-1) { static_cast<QList<QObject *>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void QmlBridgec97c40___children_setList(void* ptr, void* i)
{
	static_cast<QList<QObject *>*>(ptr)->append(static_cast<QObject*>(i));
}

void* QmlBridgec97c40___children_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject *>();
}

void* QmlBridgec97c40_NewQmlBridge(void* parent)
{
	if (dynamic_cast<QCameraImageCapture*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QCameraImageCapture*>(parent));
	} else if (dynamic_cast<QDBusPendingCallWatcher*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QDBusPendingCallWatcher*>(parent));
	} else if (dynamic_cast<QExtensionFactory*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QExtensionFactory*>(parent));
	} else if (dynamic_cast<QExtensionManager*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QExtensionManager*>(parent));
	} else if (dynamic_cast<QGraphicsObject*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QGraphicsObject*>(parent));
	} else if (dynamic_cast<QGraphicsWidget*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QGraphicsWidget*>(parent));
	} else if (dynamic_cast<QLayout*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QLayout*>(parent));
	} else if (dynamic_cast<QMediaPlaylist*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QMediaPlaylist*>(parent));
	} else if (dynamic_cast<QMediaRecorder*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QMediaRecorder*>(parent));
	} else if (dynamic_cast<QOffscreenSurface*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QOffscreenSurface*>(parent));
	} else if (dynamic_cast<QPaintDeviceWindow*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QPaintDeviceWindow*>(parent));
	} else if (dynamic_cast<QPdfWriter*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QPdfWriter*>(parent));
	} else if (dynamic_cast<QQuickItem*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QQuickItem*>(parent));
	} else if (dynamic_cast<QRadioData*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QRadioData*>(parent));
	} else if (dynamic_cast<QWidget*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QWidget*>(parent));
	} else if (dynamic_cast<QWindow*>(static_cast<QObject*>(parent))) {
		return new QmlBridgec97c40(static_cast<QWindow*>(parent));
	} else {
		return new QmlBridgec97c40(static_cast<QObject*>(parent));
	}
}

void QmlBridgec97c40_DestroyQmlBridge(void* ptr)
{
	static_cast<QmlBridgec97c40*>(ptr)->~QmlBridgec97c40();
}

void QmlBridgec97c40_DestroyQmlBridgeDefault(void* ptr)
{
	Q_UNUSED(ptr);

}

char QmlBridgec97c40_EventDefault(void* ptr, void* e)
{
	return static_cast<QmlBridgec97c40*>(ptr)->QObject::event(static_cast<QEvent*>(e));
}

char QmlBridgec97c40_EventFilterDefault(void* ptr, void* watched, void* event)
{
	return static_cast<QmlBridgec97c40*>(ptr)->QObject::eventFilter(static_cast<QObject*>(watched), static_cast<QEvent*>(event));
}

void QmlBridgec97c40_ChildEventDefault(void* ptr, void* event)
{
	static_cast<QmlBridgec97c40*>(ptr)->QObject::childEvent(static_cast<QChildEvent*>(event));
}

void QmlBridgec97c40_ConnectNotifyDefault(void* ptr, void* sign)
{
	static_cast<QmlBridgec97c40*>(ptr)->QObject::connectNotify(*static_cast<QMetaMethod*>(sign));
}

void QmlBridgec97c40_CustomEventDefault(void* ptr, void* event)
{
	static_cast<QmlBridgec97c40*>(ptr)->QObject::customEvent(static_cast<QEvent*>(event));
}

void QmlBridgec97c40_DeleteLaterDefault(void* ptr)
{
	static_cast<QmlBridgec97c40*>(ptr)->QObject::deleteLater();
}

void QmlBridgec97c40_DisconnectNotifyDefault(void* ptr, void* sign)
{
	static_cast<QmlBridgec97c40*>(ptr)->QObject::disconnectNotify(*static_cast<QMetaMethod*>(sign));
}

void QmlBridgec97c40_TimerEventDefault(void* ptr, void* event)
{
	static_cast<QmlBridgec97c40*>(ptr)->QObject::timerEvent(static_cast<QTimerEvent*>(event));
}



#include "moc_moc.h"

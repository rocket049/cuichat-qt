/****************************************************************************
** Meta object code from reading C++ file 'moc.cpp'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'moc.cpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QmlBridgec97c40_t {
    QByteArrayData data[16];
    char stringdata0[122];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QmlBridgec97c40_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QmlBridgec97c40_t qt_meta_stringdata_QmlBridgec97c40 = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QmlBridgec97c40"
QT_MOC_LITERAL(1, 16, 10), // "appendUser"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 4), // "name"
QT_MOC_LITERAL(4, 33, 10), // "removeUser"
QT_MOC_LITERAL(5, 44, 9), // "appendMsg"
QT_MOC_LITERAL(6, 54, 6), // "sender"
QT_MOC_LITERAL(7, 61, 3), // "msg"
QT_MOC_LITERAL(8, 65, 9), // "setHeader"
QT_MOC_LITERAL(9, 75, 4), // "info"
QT_MOC_LITERAL(10, 80, 11), // "sendUserMsg"
QT_MOC_LITERAL(11, 92, 5), // "login"
QT_MOC_LITERAL(12, 98, 6), // "server"
QT_MOC_LITERAL(13, 105, 7), // "channel"
QT_MOC_LITERAL(14, 113, 4), // "nick"
QT_MOC_LITERAL(15, 118, 3) // "pwd"

    },
    "QmlBridgec97c40\0appendUser\0\0name\0"
    "removeUser\0appendMsg\0sender\0msg\0"
    "setHeader\0info\0sendUserMsg\0login\0"
    "server\0channel\0nick\0pwd"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QmlBridgec97c40[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,
       4,    1,   47,    2, 0x06 /* Public */,
       5,    2,   50,    2, 0x06 /* Public */,
       8,    1,   55,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    1,   58,    2, 0x0a /* Public */,
      11,    4,   61,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,    6,    7,
    QMetaType::Void, QMetaType::QString,    9,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,    7,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString, QMetaType::QString,   12,   13,   14,   15,

       0        // eod
};

void QmlBridgec97c40::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QmlBridgec97c40 *_t = static_cast<QmlBridgec97c40 *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->appendUser((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->removeUser((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->appendMsg((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 3: _t->setHeader((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->sendUserMsg((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->login((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])),(*reinterpret_cast< QString(*)>(_a[4]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QmlBridgec97c40::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QmlBridgec97c40::appendUser)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QmlBridgec97c40::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QmlBridgec97c40::removeUser)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QmlBridgec97c40::*)(QString , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QmlBridgec97c40::appendMsg)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QmlBridgec97c40::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QmlBridgec97c40::setHeader)) {
                *result = 3;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QmlBridgec97c40::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QmlBridgec97c40.data,
    qt_meta_data_QmlBridgec97c40,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QmlBridgec97c40::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QmlBridgec97c40::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QmlBridgec97c40.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QmlBridgec97c40::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void QmlBridgec97c40::appendUser(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QmlBridgec97c40::removeUser(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QmlBridgec97c40::appendMsg(QString _t1, QString _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QmlBridgec97c40::setHeader(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

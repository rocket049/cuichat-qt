import QtQuick 2.0
import QtQuick.Controls 1.2

Rectangle {
    id:window;
    width: 800;
    height: 600;
    color: "white";
    border.color: "#808080";
    border.width: 2;
    Item{
        id:winInfo;
        x:10;
        y:10;
        width:600;
        height:40;
        Text {
            id: headInfo;
            text: qsTr("Login");
            font.pixelSize: 27;
        }
    }
    //server
    Item{
        x:10;
        y:60;
        width: 100;
        height: 20;
        Text {
            id: serverInfo;
            text: qsTr("IRC Server:");
        }
    }
    Rectangle{
        x:120;
        y:60;
        width:300;
        height: 20;
        border.width: 1;
        TextEdit{
            id:serverAddr;
            text: "chat.freenode.net:6667";
            anchors.leftMargin: 3;
            anchors.fill: parent;
        }
    }

    //channel
    Item{
        x:10;
        y:90;
        width:100;
        height:20;
        Text {
            id: channelInfo;
            text: qsTr("Room:");
        }
    }
    Rectangle{
        x:120;
        y:90;
        width: 300;
        height: 20;
        border.width: 1;
        TextEdit{
            id: channalName;
            anchors.leftMargin: 3;
            anchors.fill: parent;
        }
    }

//username
    Item{
        id:labelName;
        x:10;
        y:120;
        width:100;
        height:20;
        Text {
            id: userInfo;
            text: qsTr("UserName:");
        }
    }

    Rectangle {
        id: rectName;
        x:120;
        y:120;
        width:300;
        height:20;
        border.width: 1;

        TextInput{
            id: userName;
            anchors.fill: parent;
            anchors.leftMargin: 3;
            anchors.topMargin: 3;
            text:{
                return "xtalker"+Math.floor(Math.random()*1000).toString();
            }
        }
    }
    //password
    Item{
        id:labelPwd;
        x:10;
        y:150;
        width:100;
        height:20;
        Text {
            id: pwdInfo;
            text: qsTr("Password:");
        }
    }

    Rectangle {
        id: rectPwd;
        x:120;
        y:150;
        width:300;
        height:20;
        border.width: 1;

        TextInput{
            id: passwd;
            anchors.fill: parent;
            anchors.leftMargin: 3;
            passwordCharacter: "*";
            echoMode: TextInput.Password;
        }
    }

    Item{
        id:buttons;
        x:10;
        y:180;
        width: 430;
        height:30;
        Button{
            id:login;
            x:0;
            y:0;
            width: 60;
            text: qsTr("Login");
            onClicked: {
                console.log("login");
                windowLoader.source="qrc:/qml/chat.qml"
                QmlBridge.login(serverAddr.text,channalName.text,userName.text,passwd.text);
            }
        }
        Button{
            id:cancel;
            x:80;
            y:0;
            width: 60;
            text: qsTr("Cancel");
            onClicked: {
                console.log("cancel");
            }
        }
    }
    Loader {
           id: windowLoader;
    }
}

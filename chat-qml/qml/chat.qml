import QtQuick 2.0
import QtQuick.Controls 1.4

Rectangle {
    id:window;
    width: 800;
    height: 600;
    color: "white";
    border.color: "#808080";
    border.width: 2;
    Item{
        id:userInfo;
        x:10;
        y:10;
        width:780;
        height:20;
        Text {
            id: headInfo;
            text: qsTr("message header");
        }
    }

    ListModel {
            id: msgmode;
            onCountChanged: {
                msgList.resizeColumnsToContents()
                msgList.positionViewAtRow(msgList.rowCount-1,ListView.Center);
            }
        }

    Item {
        id:msgWin;
        x:10;
        y:40;
        width: 640;
        height:510;
        TableView{
            id:msgList;
            anchors.fill: parent;
            alternatingRowColors : true;

            TableViewColumn {
                  role: "sender";
                  title: qsTr("Sender");
                  width: 120;
                  resizable: true;
             }
              TableViewColumn {
                  role: "message";
                  title: qsTr("Message");
                  width:parent.width-120;
                  resizable: true;
              }
              model: msgmode;
              onDoubleClicked: {
                  var r = msgmode.get(row);
                  //console.log(r["sender"],r["message"]);
                  msgInput.text = r["sender"]+":"+r["message"];
              }
              horizontalScrollBarPolicy :Qt.ScrollBarAsNeeded;

              rowDelegate : Rectangle{
                  border.width: 1;
                  border.color: "#BABDB6";
                  height: 30;
              }
              itemDelegate: Text{
                  text:styleData.value;
                  font.pointSize: 14;
              }
        }
    }

    ListModel{
        id: userMode;
    }

    function addUser(uname){
        //console.log(uname);
        userMode.append({name:uname});
    }

    function removeUser(uname){
        for(var i=0;i<userMode.count;i++){
            var r = userMode.get(i);
            //console.log(r.name);
            if(changeValue(r.name) === uname){
                userMode.remove(i);
                break;
            }
        }
    }
    function changeValue( value ) {
        if (value.indexOf("@")===0)
            return value.substring(1,value.length);
        else
            return value;
    }

    function addMsg(sender,msg){
        //console.log(("from: "+sender+"\nMessage: "+msg));
        msgmode.append({sender:sender,message:msg});
    }

    function setHeader(info){
        headInfo.text=info;
    }

    Connections{
        target:QmlBridge;
        onAppendUser:addUser(name);
        onRemoveUser:removeUser(name);
        onAppendMsg:addMsg(sender,msg);
        onSetHeader:setHeader(info);
    }

    Item{
        id:userList;
        x:660;
        y:40;
        width:130;
        height:510;
        TableView{
            id:userTable;
            anchors.fill: parent;
            alternatingRowColors: false;
            TableViewColumn {
                role:"name";
                title: qsTr("User");
            }
            model:userMode;
            itemDelegate: Text{
                anchors.verticalCenter: parent.verticalCenter;
                color: changeColor(styleData.value);
                elide: styleData.elideMode;
                text: changeValue(styleData.value);
                function changeColor( value ) {
                    if (value.indexOf("@")===0)
                        return "red";
                    else
                        return "black";
                 }
                function changeValue( value ) {
                    if (value.indexOf("@")===0)
                        return value.substring(1,value.length);
                    else
                        return value;
                }
            }
        }
    }

    Item {
        id: viewLabel;
        x:10;
        y:560;
        width:30;
        height:25;
        Text {
            id: msglabel;
            text: qsTr("Say:");
        }
    }
    Rectangle {
        id: viewInput;
        x:50;
        y:560;
        width:700;
        height:25;
        border.width: 1;
        function textAccepted(){
            QmlBridge.sendUserMsg(msgInput.text)
            msgInput.text="";
        }

        TextInput{
            id: msgInput;
            anchors.fill: parent;
            anchors.leftMargin: 3;
            anchors.topMargin: 3;
            onAccepted: parent.textAccepted();
        }
    }
}

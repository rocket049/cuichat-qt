package main

import (
	"fmt"
	"strings"
	"time"
)

func init() {
	verbose = true
	step = 1
}

var (
	buf     string
	bot     *Bot
	target  string
	step    int
	verbose bool
)

func isZero(s string) bool {
	return len(s) == 0
}

func doLogin(server, channel, nick, pwd string, s *QmlBridge) string {
	if isZero(server) || isZero(channel) || isZero(nick) || isZero(pwd) {
		panic("error input")
	}
	bot = new(Bot)
	bot.server = server
	bot.User = nick
	bot.Nick = bot.User
	bot.Channel = fmt.Sprintf("#xtalk%s", channel)

	target = bot.Channel

	bot.Crypto.SetKey(pwd)

	info := fmt.Sprintf("%s / %s / %s", server, target, nick)

	step = 2

	go doRecv(s)
	return info
}

//doRecv work in goroutine
func doRecv(s *QmlBridge) {
	bot.Connect()

	bot.Command("MODE", bot.Channel, "+s")
	for {
		message, err := bot.Recv()
		if err != nil {
			s.AppendMsg("sys", "Exit Now!")
			break
		}
		//fmt.Println(message.Command, message.Params)

		if message.Command == "JOIN" {
			//connected
			s.AppendMsg(message.Prefix.Name, message.Command)
			if step < 3 && message.Prefix.Name == bot.Nick {
				step = 3
			}
			if message.Prefix.Name != bot.Nick {
				s.AppendUser(message.Prefix.Name)
			}
		} else if message.Command == "QUIT" {
			s.AppendMsg(message.Prefix.Name, message.Command)
			s.RemoveUser(message.Prefix.Name)
		} else if message.Command == "PING" {
			bot.Send(fmt.Sprintf("PONG %d", time.Now().UnixNano()))
			//log.Println("SEND: PONG")
		} else if message.Command == "PRIVMSG" {
			// Do Something with this msg
			rmsg := message.Params[1]
			dmsg := bot.Crypto.Decode(rmsg)
			name1 := message.Prefix.Name
			var msg string
			if dmsg == nil {
				msg = fmt.Sprintf("%s %s", "[not secret]", rmsg)
				s.AppendMsg(string(name1), msg)
			} else {
				msg = string(dmsg)
				s.AppendMsg(string(name1), msg)
			}
		} else if message.Command == "353" {
			names := strings.Split(message.Params[3], " ")
			for _, v := range names {
				s.AppendUser(v)
			}
		} else if verbose {
			//log.Printf("%v\n", message)
			msg := fmt.Sprintf("%s:%s", message.Command, strings.Join(message.Params, ","))
			//log.Println(msg)
			s.AppendMsg(message.Prefix.Name, msg)
		}
	}
}

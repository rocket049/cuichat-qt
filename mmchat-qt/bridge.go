package main

import (
	"github.com/therecipe/qt/core"
)

type QmlBridge struct {
	core.QObject

	_ func(data string)                       `slot:"sendUserMsg"`
	_ func(server, channel, nick, pwd string) `slot:"login"`

	_ func(info string)        `signal:"setHeader"`
	_ func(name string)        `signal:"appendUser"`
	_ func(name string)        `signal:"removeUser"`
	_ func(sender, msg string) `signal:"appendMsg"`
}

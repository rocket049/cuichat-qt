package main

import (
	"os"
	"path/filepath"

	"github.com/therecipe/qt/gui"

	"github.com/therecipe/qt/widgets"
)

func getIconPath() string {
	exe1, _ := os.Executable()
	dir1 := filepath.Dir(exe1)
	return filepath.Join(dir1, "..", "icons", "internet.png")
}

func (s *myWindow) showSystemTray() {
	s.tray = widgets.NewQSystemTrayIcon(s.window)

	icon := gui.NewQIcon5(":/qml/icons/internet.png")

	s.tray.SetIcon(icon)
	s.tray.SetToolTip(T("Security Chat by IRC"))

	menu := widgets.NewQMenu(s.window)
	actShow := menu.AddAction(T("Show"))
	actShow.SetToolTip(T("Security Chat by IRC"))
	actQuit := menu.AddAction(T("Quit"))

	actShow.ConnectTriggered(func(checked bool) {
		s.window.Show()
	})

	actQuit.ConnectTriggered(func(checked bool) {
		s.window.DisconnectCloseEvent()
		s.window.Close()
	})

	s.tray.SetContextMenu(menu)

	s.tray.Show()
}

func (s *myWindow) showTrayMessage(sender, msg string) {
	if s.window.IsHidden() == false {
		return
	}
	s.tray.ShowMessage2(sender, msg, s.tray.Icon(), 5000)
}

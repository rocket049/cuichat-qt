v1.1.0 修改了密码形成算法，与 v1.0.x 版不兼容。

## 这是什么软件？

what is it?

一个加密聊天软件，`AES256`加密，通过`IRC`服务器互联。
注：用户名、聊天室名只能用字母和数字
This is a crypto chat software, crypto method: AES256, link by IRC Server.

## GUI模式和CONSOLE模式
本程序通过`plugin`实现了同时支持窗口模式和终端模式。无参数运行时进入gui模式，带参数`-console`启动时进入终端模式

## 编译安装
```
git clone --depth 1 github.com/rocket049/cuichat
cd cuichat
go build
cd plugins
go build -buildmode=plugin ./cui
go build -buildmode=plugin ./gui
cd ..
#run in gui mode
./cuichat
#run in console mode
./cuichat -console
```

## console ui 模式功能键

- 上 => 把前一行字复制到输入框内
- 下 => 把下一行字复制到输入框内
- PgUp => 向上翻页
- PgDn => 向下翻页

## 说明

通信协议和 `https://gitee.com/rocket049/libmychat` 兼容。
